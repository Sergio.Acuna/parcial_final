import express from "express";

let app = express();

//Rutas

app.get("/promedio",(req, res) => {

    let miCalculadorBasico = new CalculadorBasico();
    
    miCalculadorBasico.valor_A = 44;
    miCalculadorBasico.valor_B = 78;
    miCalculadorBasico.valor_C = 25; 

    let respuesta = miCalculadorBasico.promedio(); 
    res.send(respuesta.toString());

});


app.get("/sumatoria",(req, res) =>{

    let miCalculadorBasico = new CalculadorBasico();

    miCalculadorBasico.valor_A = 50;
    miCalculadorBasico.valor_B = 85;
    miCalculadorBasico.valor_C = 20; 

    let respuesta = miCalculadorBasico.sumatoria();
    res.send(respuesta.toString());
    
});

app.get("/menor",(req, res) => {

    let miCalculadorAvanzado = new CalculadorAvanzado();

    miCalculadorAvanzado.valor_A = 2;
    miCalculadorAvanzado.valor_B = 5;
    miCalculadorAvanzado.valor_C = 7;

    let respuesta = miCalculadorAvanzado.menor();
    res.send(respuesta.toString());
    
});

app.get("/mayor",(req, res) => {

    let miCalculadorAvanzado = new CalculadorAvanzado();

    miCalculadorAvanzado.valor_A = 2;
    miCalculadorAvanzado.valor_B = 5;
    miCalculadorAvanzado.valor_C = 7;

    let respuesta = miCalculadorAvanzado.mayor();    
    res.send(respuesta.toString());

});

app.listen(3000);

class Calculador{

    valor_A;
    valor_B;
    valor_C;
    resultado;

    constructor(valor_A, valor_B, valor_C, resultado){

        this.valor_A = valor_A;
        this.valor_B = valor_B;
        this.valor_C = valor_C;
        this.resultado = resultado;
        
    }

}

class CalculadorBasico extends Calculador{


promedio(){

    this.resultado = ((this.valor_A+this.valor_B+this.valor_C)/3);
    return this.resultado;
}
sumatoria(){

    this.resultado = this.valor_A+this.valor_B+this.valor_C;
    return this.resultado;

}  

}

class CalculadorAvanzado extends Calculador{

    menor(){

        if (this.valor_A<=this.valor_B && this.valor_A<=this.valor_C) 
        {
           return this.valor_A;
        }
        else if(this.valor_B<this.valor_A && this.valor_B<this.valor_C) 
        {
           return this.valor_B;
        }
        else 
        {
           return this.valor_C;
        }

    }
    mayor(){

        if (this.valor_A>=this.valor_B && this.valor_A>=this.valor_C) 
        {
           return this.valor_A;
        }
        else if(this.valor_B>this.valor_A && this.valor_B>this.valor_C) 
        {
           return this.valor_B;
        }
        else 
        {
           return this.valor_C;
        }
    }
}